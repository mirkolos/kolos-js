kolos.component.Modal = function() {
    // base properties -->>
    let Self = this;
    /** @type {kolos.ComponentContext} */
    this.context = undefined;
    this.element = {
        content: undefined,
    };
    this.component = {
        /** @type {kolos.component.Hint} */
        hint: undefined,
    }
    this.param = {};
    this.attr = {};
    //--

    this.__isMouseDown = false;
    this.__onClickBack = undefined;

    this.onClickBack = function (callback) {
        this.__onClickBack = callback;
    }

    this.__clickBack = function () {
        // если нажатие было не фоне, то ничего не делаем
        if (Self.__isMouseDown) {
            Self.__isMouseDown = false;
            // выполняем экшины
            if (Self.__onClickBack !== undefined) {
                Self.__onClickBack(Self);
            } else {
                Self.close();
            }
        }
    }

    this.__mouseDown = function () {
        Self.__isMouseDown = true;
    }

    this.show = function () {
        kolos.Modal.zIndex++;
        $(Self.context.element).css('z-index', kolos.Modal.zIndex);
        //выравниваем по центу
        kolos.Utils.toCenterTopMargin(Self.element.content);
        //отображаем
        $(Self.context.element).fadeIn(50);
        //выравниваем по центу
        kolos.Utils.toCenterTopMargin(Self.context.element);
    }


    this.hide = function () {
        kolos.Modal.zIndex--;
        $(this.context.element).fadeOut(50);
    }

    this.close = function () {
        Self.hide();
    }

    this.onReady = function() {
        //
    }

    this.onDestroy = function() {
        this.hide();
    }

    this.template = function() {
        return `
            <div class="modal-back" style="display: none;" onclick="kolos.component.Modal.__clickBack();" onmousedown="kolos.component.Modal.__mouseDown();">
                <div id="content" class="modal-content" onclick="event.stopPropagation();" onmousedown="event.stopPropagation();">[[content]]</div>
            </div>
        `;
    }

}

