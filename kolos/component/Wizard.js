kolos.component.Wizard = function() {
    // base properties -->>
    let Self = this;
    /** @type {kolos.ComponentContext} */
    this.context = {};
    this.element = {
        tabs: {},
        btnNext: {},
        btnPrev: {},
    };
    this.component = {
    }
    this.param = {};
    this.attr = {};
    //--


    this.steps = {};
    this.currentStep = 1;

    this.next = function () {
        let count = kolos.Utils.getLength(Self.steps);
        let index = this.currentStep;
        if (index < count) {
            this.change(++index);
        }
    }

    this.prev = function () {
        let index = this.currentStep;
        if (index > 1) {
            this.change(--index);
        }
    }

    this.change = function (stepIndex) {
        // отрабатываем событие выхода из шага
        let lastStep = Self.steps[Self.currentStep];
        if (lastStep._after !== undefined) {
            let result = lastStep._after(lastStep);
            // если проверка завершилась неудачей, не даём переходить на следующий шаг
            if (
                stepIndex > Self.currentStep
                && result !== undefined
                && result === false
            ) {
                return;
            }
        }
        // новый шаг
        Self.currentStep = stepIndex;
        // отрабатываем событие входа в шаг
        let curStep = Self.steps[Self.currentStep];
        if (curStep._before !== undefined) {
            curStep._before(curStep);
        }
        Self.update();
    }

    this.update = function () {
        let count = kolos.Utils.getLength(this.steps);

        for (let i in this.steps) {
            let item = this.steps[i];
            if (i < this.currentStep) {
                item.setStateSuccess();
                item.hide();
                if (item._after !== undefined) {
                    item._after(item);
                }
            }
            if (i == this.currentStep) {
                item.setStateActive();
                item.show()
            }
            if (i > this.currentStep) {
                item.setStateNone();
                item.hide();
            }
        }

        // обновляем кнопки
        if (this.currentStep >= count) {
            $(this.element.btnNext).hide();
        } else {
            $(this.element.btnNext).show();
        }
        //--
        if (this.currentStep <= 1) {
            $(this.element.btnPrev).hide();
        } else {
            $(this.element.btnPrev).show();
        }

    }

    /**
     *
     * @param index
     * @returns {kolos.component.Wizard.Step}
     */
    this.getStep = function (index) {
        return Self.steps[index];
    }



    this.onPage = function () {
    }

    this.onInit = function () {
    }

    this.onReady = function() {
        let tabElements = kolos.Html.getFirstChildren(this.element.content);

        // скрываем все элементы
        for (let i = 0; i < tabElements.length; i++) {
            $(tabElements[i]).hide();
        }

        // создаём шаги
        let index = 1;
        for (let i in tabElements) {
            this.steps[index] = new kolos.component.Wizard.Step(this, tabElements[i], index++);
        }

        // засовываем вкладки в контейнер вкладок
        for (let i in this.steps) {
            let step = this.steps[i];
            $(this.element.tabs).append(step.tabElement);
        }

        // показываем первую вкладку
        this.change(1);
    }

    this.onDestroy = function() {
    }

    this.template = function () {
        return `<div>
            <style>
                .stepper-item {
                    display: inline-block;
                    border-bottom: 3px solid var(--c-disabled);
                    padding: var(--p-norm);
                    color: var(--c-disabled);
                    margin-right: var(--m-norm);
                }
                .stepper-item .sti-success {
                    display: none;
                }
                .stepper-item .sti-error {
                    display: none;
                }
        
                .stepper-item.active {
                    border-color: #02bbff;
                    background: #f2faff;
                    color: var(--c-base);
                }
        
                .stepper-item.success {
                    border-color: var(--c-success);
                    color: var(--c-base);
                }
                .stepper-item.success .sti-success {
                    display: inline-block;
                }
        
                .stepper-item.error {
                    border-color: var(--c-error);
                    color: var(--c-base);
                }
                .stepper-item.error .sti-error {
                    display: inline-block;
                }
        
                .stepper-line {
                    border-top: 1px solid #e9e9e9;
                    margin-top: -1px;
                }
            </style>
        
            <div>
                <div id="tabs"></div>
                <div class="stepper-line"></div>
            </div>
        
            <div id="content" class="m-norm mt-big">
                [[content]]
            </div>
        
            <div class="m-norm mt-big mb-big">
                <a id="btnPrev" class="button" style="display: none;" href="javascript: kolos.component.Wizard.prev()">
                    <i class="icon-m c-base">arrow_back</i>
                    Назад
                </a>
                <a id="btnNext" class="button active" style="display: inline-block;" href="javascript: kolos.component.Wizard.next()">
                    Далее
                    <i class="icon-m c-white">arrow_forward</i>
                </a>
            </div>
        </div>`;
    }
}


kolos.component.Wizard.Step = function (component, itemElement, index) {
    let Self = this;
    this.index = index;
    this.component = component;
    this.itemElement = itemElement;
    this.tabElement = undefined;
    //--
    /** @type {kolos.Form} */
    this.form = new kolos.Form(this.itemElement);
    this.title = 'Шаг ' + index;
    this.caption = '';
    this._before = undefined;
    this._after = undefined;

    //--

    this.init = function () {
        let title = $(this.itemElement).attr('title');
        if (title) {
            this.title = title;
        }
        let caption = $(this.itemElement).attr('caption');
        if (caption) {
            this.caption = caption;
        }
    }
    this.init();

    this.before = function (callback) {
        this._before = callback;
    }

    this.after = function (callback) {
        this._after = callback;
    }

    this.show = function () {
        $(this.itemElement).fadeIn();
    }

    this.hide = function () {
        $(this.itemElement).hide();
    }

    this.setStateNone = function () {
        $(Self.tabElement).attr('class', 'stepper-item');
        $(Self.tabElement).removeAttr('tooltip');
    }

    this.setStateActive = function () {
        $(Self.tabElement).attr('class', 'stepper-item active');
        $(Self.tabElement).removeAttr('tooltip');
    }

    this.setStateSuccess = function () {
        $(Self.tabElement).attr('class', 'stepper-item success');
        $(Self.tabElement).removeAttr('tooltip');
    }

    this.setStateError = function (msg) {
        $(Self.tabElement).attr('class', 'stepper-item error');
        if (msg != undefined) {
            $(Self.tabElement).attr('tooltip', '⚠️ ' + msg);
        } else {
            $(Self.tabElement).removeAttr('tooltip');
        }
    }

    this.template = function () {
        return `<a class="stepper-item success dn" href="javascript: kolos.component.Wizard.change(` + this.index + `)">
                <span>` + this.title + `</span> 
                <span class="sti-success"><i class="icon-m c-success text-norm">done</i></span>
                <span class="sti-error"><i class="icon-m c-error text-norm">warning_amber</i></span><br>
                <div class="text-small">` + this.caption + `</div>
            </a>
        `;
    }

    // создаём таб
    this.tabElement = $( component.render(this.template()) )[0];
}

