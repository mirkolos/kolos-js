kolos.component.PasswordChecker = function () {
    let Self = this;
    this.component = {
        /** @type {kolos.component.ProgressBar} */
        progressBar: {}
    }
    this.element = {
        caption1: {},
        caption2: {}
    }

    this.minStrength = 2;

    this._input1 = undefined;
    this._input2 = undefined;
    this.__noEquals = false;

    this.onReady = function () {
        this.component.progressBar.setMin(0);
        this.component.progressBar.setMax(4);
        this.component.progressBar.setProgress(0, '');
    }

    this.check = function (pass, pass2) {
        return Self.__checkProgress(pass)
            && Self.__checkEquals(pass, pass2);
    }

    this.checkStrength = function (pass) {
        return Self.__checkStrength(pass) >= Self.minStrength;
    }

    this.__checkProgress = function (pass) {
        let strength = Self.__checkStrength(pass);
        let state = Self.__getStateForStrength(strength);
        $(Self.element.caption1).html('<span class="c-' + state + '">' + Self.__getCaptionForStrength(strength) + '</span>');
        Self.component.progressBar.setProgress(strength, '');
        Self.component.progressBar.setType(state);
        return this.checkStrength(pass);
    }

    this.__checkEquals = function (pass, pass2) {
        if (pass != pass2) {
            $(Self.element.caption2).html('<span class="caption-small c-error"><i class="icon-m">close</i> Пароли не совпадают</span>');
            Self.__noEquals = true;
            return true;
        } else {
            $(Self.element.caption2).html('');
        }
        return false;
    }

    this.attachInputs = function (inputMain, inputSecond) {
        this._input1 = inputMain;
        this._input2 = inputSecond;
        $(Self._input1).on("input",function(){
            Self.__checkProgress($(Self._input1).val());
        });
        $(Self._input1).focus(function(){
            $(Self._input1).removeClass('error');
        });
        $(Self._input1).blur(function(){
            if (!Self.checkStrength($(Self._input1).val())) {
                $(Self._input1).addClass('error');
            }
        });

        if (Self._input2 !== undefined) {
            $(Self._input2).on("input",function(){
                // если был потерян фокус с несовпадающим паролем, то проверяем постоянно
                if (Self.__noEquals) {
                    Self.__checkEquals($(Self._input1).val(), $(Self._input2).val());
                }
            });
            $(Self._input2).focus(function(){
                $(Self._input2).removeClass('error');
            });
            $(Self._input2).blur(function(){
                Self.__checkEquals($(Self._input1).val(), $(Self._input2).val());
            });
        }
    }

    this.__getCaptionForStrength = function (strength) {
        if (strength <= 1) {
            return '<i class="icon-m">close</i> Слишком слабый пароль';
        } else if (strength == 2) {
            return '<i class="icon-m">done</i> Слабый пароль';
        } else if (strength == 3) {
            return '<i class="icon-m">done</i> Нормальный пароль';
        } else if (strength >= 4) {
            return '<i class="icon-m">done</i> Отличный пароль';
        }
        return '';
    }

    this.__getStateForStrength = function (strength) {
        if (strength < Self.minStrength) {
            return 'error';
        }
        if (strength <= 1) {
            return 'error';
        } else if (strength == 2) {
            return 'warning';
        } else if (strength == 3) {
            return 'info';
        } else if (strength >= 4) {
            return 'success';
        }
        return '';
    }

    this.__checkStrength = function(password) {
        // Минимальное количество символов для безопасного пароля
        const minLength = 8;
        // Регулярные выражения для проверки наличия разных типов символов
        const lowercaseRegex = /[a-z]/;
        const uppercaseRegex = /[A-Z]/;
        const numberRegex = /[0-9]/;
        const specialCharRegex = /[!@#$%^&*()_+\-\[\]{};:'"\\|,.<>\/?]/;
        let strength = 0;
        // Проверка длины пароля
        if (password.length >= minLength) {
            strength++;
        }
        // Проверка наличия строчных и прописных букв
        if (lowercaseRegex.test(password) && uppercaseRegex.test(password)) {
            strength++;
        }
        // Проверка наличия цифр
        if (numberRegex.test(password)) {
            strength++;
        }
        // Проверка наличия специальных символов
        if (specialCharRegex.test(password)) {
            strength++;
        }
        // Возвращение силы пароля в виде числа от 0 до 4
        return strength;
    }

    this.template = function () {
        return `<div style="width: 300px;">
                        <div class="caption-small">
                            <div id="caption1"></div>
                            <div id="caption2"></div>
                        </div>
                        <div component="kolos.component.ProgressBar"></div>
                </div>`;
    }
}