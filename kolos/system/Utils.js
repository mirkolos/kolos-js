JSON.safeStringify = (obj, indent = 2) => {
    let cache = [];
    const retVal = JSON.stringify(
        obj,
        (key, value) =>
            typeof value === "object" && value !== null
                ? cache.includes(value)
                    ? undefined
                    : cache.push(value) && value
                : value,
        indent
    );
    cache = null;
    return retVal;
};


kolos.Utils = {

    __debug: false,
    __delayRequest: false,
    __verRand: false,
    __ver: '',
    __verCore: '1.2',
    __loadJs: {},
    __loadCss: {},
    __delayed: {},

    val: function (arr, key, def = undefined) {
        if (arr === undefined || arr[key] === undefined) {
            return def;
        }
        return arr[key];
    },

    /**
     * Извлечь значение из вложенных объектов по иерархии ключей,
     * где ключ "attr1.attr2.attr3"
     */
    extractFromHierarchyKey: function(obj, key, def = undefined) {
        let keys = key.split('.');
        let subObj = obj;
        for (let i in keys) {
            let key = keys[i];
            subObj = subObj[key];
            if (subObj === undefined) {
                return def;
            }
        }
        return subObj;
    },

    /**
     * Задать значение для вложенных объектов по иерархии ключей,
     * где ключ "attr1.attr2.attr3"
     */
    setForHierarchyKey: function(obj, key, value) {
        let keys = key.split('.');
        let subObj = obj;
        // достаём вложенный объект, кроме последнего, это атрибут
        for (let i = 0; i < keys.length - 1; i++) {
            let key = keys[i];
            if (subObj[key] === undefined) {
                subObj[key] = {};
            }
            subObj = subObj[key];
        }
        let lastKey = keys[keys.length - 1];
        subObj[lastKey] = value
    },

    contains: function (data, value) {
        for (let i in data) {
            if (value == data[i]) {
                return true;
            }
        }
        return false;
    },

    containsKey: function (data, key) {
        for (let i in data) {
            if (key == i) {
                return true;
            }
        }
        return false;
    },

    getFirst: function (data) {
        for (let i in data) {
            return data[i];
        }
        return undefined;
    },

    setDebugMode: function (val) {
        if (val === undefined) {
            val = true;
        }
        kolos.Utils.__debug = val;
        kolos.Utils.__verRand = val
    },

    setVersion: function (ver) {
        kolos.Utils.__ver = '-' + ver;
    },

    getVer: function () {
        if (kolos.Utils.__verRand) {
            return kolos.Utils.random(0, 99999);
        }
        return kolos.Utils.__verCore + kolos.Utils.__ver;
    },

    /**
     * Load script and call executor after loading
     * @param src
     * @param executor
     */
    loadScriptAndExec: function(src, executor) {

        /**
         * Контекст загрузки скрипта
         * @param src
         */
        let ScriptContext = function (src) {
            let Self = this;
            this.callbackList = [];
            this.isAdd = false;
            this.isLoad = false;

            this.addExecutor = function (callback) {
                this.callbackList.push(callback);
            }

            this.execute = function() {
                // создаём загрузчик скрипт, если не был создан
                this.__createLoader();
                // выполняем колбэки, если скрипт уже загружен
                this.__executeCallback();
            }

            this.__createLoader = function () {
                if (!this.isAdd) {
                    this.isAdd = true;
                    // добавляем скрипт на загрузку
                    let script = document.createElement('script');
                    let head = document.getElementsByTagName('head')[0];
                    script.type = 'text/javascript';
                    script.src  = src + '?v=' + kolos.Utils.getVer();
                    script.onload = function() {
                        Self.isLoad = true;
                        kolos.Utils.debug('Script loaded: ' + src);
                        // выполняем колбэки
                        Self.__executeCallback();
                    }
                    script.onerror = function() {
                        kolos.Utils.error('Failed load: ' + script.src)
                    };
                    head.appendChild(script);
                }
            }

            this.__executeCallback = function () {
                if (this.isLoad) {
                    let list = Self.callbackList;
                    // затираем колбэки, и считаем их выполнеными
                    Self.callbackList = [];
                    // выполняем каждый колбэки
                    for (let i in list) {
                        let callback = list[i];
                        try {
                            callback();
                        } catch (e) {
                            kolos.Utils.error(e);
                        }
                    }
                }
            }
        }

        if (this.__loadJs[src] === undefined) {
            this.__loadJs[src] = new ScriptContext(src);
        }

        let context = this.__loadJs[src];
        if (executor !== undefined) {
            context.addExecutor(executor);
        }
        context.execute();
    },

    loadScripts: function (urlArr, callback, rootPath = "") {
        let Self = kolos.Utils;

        // добавляем корневой путь к скриптам
        for (let i in urlArr) {
            urlArr[i] = Self.addLastSlash(rootPath) + urlArr[i];
        }

        let ScriptBatch = function (urlArr, callback) {
            this.callback = callback;
            this.count = urlArr.length;
            this.counter = 0;

            this.handlerLoad = function() {
                this.counter++;
                if (this.counter >= this.count) {
                    this.callback(urlArr);
                }
            }
        }

        let batch = new ScriptBatch(urlArr, callback);

        for (let i in urlArr) {
            let url = urlArr[i];
            Self.loadScriptAndExec(url, () => {
                batch.handlerLoad();
            });
        }
    },

    loadCss: function (src) {
        if (this.__loadCss[src] !== undefined) {
            return;
        }
        this.__loadCss[src] = true;
        //--
        let link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = src + '?v=' + kolos.Utils.getVer();
        link.media = 'all';
        //link.setAttribute('onload', 'alert("asdfsf")');
        document.getElementsByTagName('HEAD')[0].appendChild(link);
    },

    rawurlencode: function(str){
        str = (str + '').toString();
        return escape(encodeURIComponent(str));
    },

    rawurldecode: function(str){
        str = (str + '').toString();
        return decodeURIComponent(unescape(str));
    },

    objToParams: function(params) {
        var arrTmp = [];
        for (var key in params) {
            arrTmp.push(key + '=' + rawurlencode(params[key]));
        }
        return arrTmp.join('&');
    },

    setCookie: function(name, value, expires, path, domain, secure) {
        // name, value - обязательные параметры
        document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
    },

    getCookie: function(name) {
        var cookie = " " + document.cookie;
        var search = " " + name + "=";
        var setStr = null;
        var offset = 0;
        var end = 0;
        if (cookie.length > 0) {
            offset = cookie.indexOf(search);
            if (offset != -1) {
                offset += search.length;
                end = cookie.indexOf(";", offset)
                if (end == -1) {
                    end = cookie.length;
                }
                setStr = unescape(cookie.substring(offset, end));
            }
        }
        return(setStr);
    },

    getBrowser: function(){
        var ua = navigator.userAgent;
        if (ua.match(/MSIE/)) return 'IE';
        if (ua.match(/Firefox/)) return 'Firefox';
        if (ua.match(/Opera/)) return 'Opera';
        if (ua.match(/Chrome/)) return 'Chrome';
        if (ua.match(/Safari/)) return 'Safari';
    },

    varDump: function(object) {
        var out = "";
        if(object && typeof(object) === "object"){
            for (var i in object) {
                out += i + ": " + object[i] + "\n";
            }
        } else {
            out = object;
        }
        /*if (console !== undefined) {
            console.log("varDump: " + out);
        }*/
        return out;
    },

    isEmpty: function(obj) {
        if (obj == undefined || obj == 'undefined' || obj == null || obj == '') {
            return true;
        }
        if (typeof(obj) === "object") {
            for (var propName in obj) {
                if (obj.hasOwnProperty(propName)) {
                    return false;
                }
            }
            return true;
        } else if (Array.isArray(obj)) {
            return obj.length === 0;
        }
        return false;
    },

    //отложенный запуск по условию
    checkExec: function(funcCheck, funcExec, timeout, funcTimeout) {
        if (timeout === undefined) {
            timeout = 10000; //таймаут по умолчанию
        }
        //задаём таймаут
        var timeoutVar = kolos.Date.setTimeout(timeout);
        //запускаем таймер
        var timerInterval = setInterval(function() {
            //если проверка прошла успешно
            if (funcCheck()) {
                clearInterval(timerInterval);
                funcExec();
            }
            //если вышел таймаут
            if (kolos.Date.checkTimeout(timeoutVar)) {
                clearInterval(timerInterval);
                if (funcTimeout !== undefined) {
                    funcTimeout();
                }
            }
        }, 200);
    },

    delayedExec: function(key, timeout, callback) {
        if (this.__delayed[key] !== undefined) {
            clearTimeout(this.__delayed[key]);
        }
        this.__delayed[key] = setTimeout(() => {
            delete this.__delayed[key];
            callback();
        }, timeout);
    },

    toCenter: function(tagId) {
        var elementJQ = $(tagId);
        var windowJQ = $(window);
        elementJQ.css("position","absolute");
        elementJQ.css("top", Math.max(0, ((windowJQ.height() - $(elementJQ).outerHeight()) / 3) +
            windowJQ.scrollTop()) + "px");
        elementJQ.css("left", Math.max(0, ((windowJQ.width() - $(elementJQ).outerWidth()) / 2) +
            windowJQ.scrollLeft()) + "px");
    },

    toCenterTop: function(tagId, percent) {
        if (percent === undefined) {
            percent = 30;
        }
        var elementJQ = $(tagId);
        var windowJQ = $(window);
        elementJQ.css("position","absolute");
        elementJQ.css("top", Math.max(0, ((windowJQ.height() - $(elementJQ).outerHeight()) / (100 / percent)) +
            windowJQ.scrollTop()) + "px");
        elementJQ.css("left", Math.max(0, ((windowJQ.width() - $(elementJQ).outerWidth()) / 2) +
            windowJQ.scrollLeft()) + "px");
    },

    toCenterTopMargin: function(tagId, percent) {
        if (percent === undefined) {
            percent = 30;
        }
        var elementJQ = $(tagId);
        var windowJQ = $(window);
        elementJQ.css(
            "margin-top",
            Math.max(0, ((windowJQ.height() - $(elementJQ).outerHeight()) / (100 / percent)) + windowJQ.scrollTop()) + "px"
        );
    },

    firstUp: function(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },

    firstLow: function(str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    },

    redirect: function (url) {
        document.location.href = url;
    },

    reload: function () {
        document.location.reload();
    },

    /**
     *
     * @param url
     * @returns {kolos.Request}
     */
    request: function (url) {
        return new kolos.Request(url);
    },

    /**
     * Используется для глобальной локализации
     * @param text
     * @returns string
     */
    text: function (text) {
        return text;
    },

    error: function (msg) {
        console.error('Error: ' + msg);
        if (this.isObject(msg)) {
            console.error(msg);
        }
        if (kolos.app.hint !== undefined) {
            kolos.app.hint.error(msg);
        }
    },

    debug: function (msg) {
        if (this.__debug) {
            console.log(msg);
        }
    },

    isObject: function (value) {
        return value && (typeof value === 'object');
    },

    random: function (min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    },

    merge: function (objBase, objPriority) {
        let result = {};
        for (let key in objBase) {
            result[key] = objBase[key];
        }
        for (let key in objPriority) {
            result[key] = objPriority[key];
        }
        return result;
    },

    copy: function (obj) {
        let result = {};
        for (let key in obj) {
            result[key] = obj[key];
        }
        return result;
    },

    setFrom: function (target, source, fields) {
        if (source === undefined) {
            return;
        }
        // если объект пустой, инициализируем ему поля
        if (this.isEmpty(target)) {
            if (fields !== undefined) {
                for (let i in fields) {
                    let field = fields[i];
                    target[field] = undefined;
                }
            } else {
                for (let field in source) {
                    target[field] = undefined;
                }
            }
        }
        // если поля для копирования не заданы, то достаём их из объекта назначения
        if (fields === undefined) {
            fields = this.extractKeys(target);
        }
        for (let i in fields) {
            let field = fields[i];
            target[field] = source[field];
        }
    },

    extractKeys: function (data) {
        let fields = [];
        for (let field in data) {
            fields.push(field);
        }
        return fields;
    },

    getLength: function (obj) {
        if (obj == null) {
            return 0;
        }
        if (obj instanceof Array) {
            return obj.length;
        } else {
            let count = 0;
            for (let i in obj) {
                count++;
            }
            return count;
        }
    },

    /**
     * Преобразует строковое значение во вложенные объекты
     * 'name1.name2.name3' -> window['name1']['name2']['name3'] = {}
     * @param strNamespace
     */
    initNamespace: function (strNamespace) {
        let spaces = strNamespace.split('.');
        if (spaces.length === 0) {
            return undefined;
        }
        // задаём корневое пространство
        let parentSpace = window;
        // начинаем проверку со следующего пространства
        for (let i = 0; i < spaces.length; i++) {
            let space = spaces[i];
            if (parentSpace[space] === undefined) {
                // создаём пространство имён
                parentSpace[space] = {};
            }
            // запоминаем родительское пространство имён
            parentSpace = parentSpace[space];
        }
        return parentSpace;
    },

    parseBoolean: function (val) {
        if (val === true) {
            return true;
        }
        if (val === false) {
            return true;
        }
        //val = (val + ' ').trim();
        if (val == 'true') {
            return true;
        }
        if (val == 'false') {
            return false;
        }
        return false;
    },

    addFirstSlash: function (path) {
        if (
            path !== undefined
            && path !== ''
            && path[0] !== '/'
        ) {
            return '/' + path;
        }
        return path;
    },

    addLastSlash: function (path) {
        if (
            path !== undefined
            && path !== ''
            && path[path.length - 1] !== '/'
        ) {
            return path + '/';
        }
        return path;
    },

    createProxy: function (object) {
        if (object.__listener__ === undefined) {
            function ProxyListener () {
                let Self = this;
                this.listeners = {};
                this.clear = function () {
                    this.listeners = {};
                }
                this.add = function (field, callback) {
                    if (Self.listeners[field] === undefined) {
                        Self.listeners[field] = [];
                    }
                    Self.listeners[field].push(callback);
                }
                this.exec = function (fieldEx, value) {
                    if (Self.listeners[fieldEx] !== undefined) {
                        let list = Self.listeners[fieldEx];
                        for (let i in list) {
                            list[i](value);
                        }
                    }
                }
            }
            // создаём слушатель значений
            let listener = new ProxyListener();
            // создаём прокси для значений
            let proxy = new Proxy(object, {
                get(target, prop) {
                    // костыль, чтобы получить прикреплённый слушатель
                    if (prop == '__listener__') {
                        return listener;
                    }
                    return target[prop];
                },
                set(target, prop, val) {
                    target[prop] = val;
                    listener.exec(prop, val);
                    return true;
                },
            });
            // заменяем объект на прокси, теперь это прокси со встроенным слушателем
            object = proxy;
        }
        return object;
    },

    listenValue: function (object, field, callback) {
        if (object.__listener__ === undefined) {
            object = this.createProxy(object);
        }
        object.__listener__.add(field, callback);
        if (!object.hasOwnProperty(field)) {
            kolos.Utils.error('listenValue. Свойство ' + field + ' отсутствует');
        }
        return object;
    },

    isTrue: function (value) {
        if (typeof value === 'boolean') {
            return value;
        } else if (typeof value === 'string' && (
            value.toLowerCase() === 'true'
            || value === '1'
            || value.toLowerCase() === 'on'
        )) {
            return true;
        } else if (typeof value === 'number' && value === 1) {
            return true;
        }
        return false;
    },

    attachElementToModel: function(element, object, modelName) {
        // если задана модель, то подключаем элемент к моделе
        if (modelName !== undefined) {
            if (element.tagName == 'INPUT'
                || element.tagName == 'TEXTAREA'
                || element.tagName == 'SELECT'
            ) {
                let arrModel = modelName.split('.');
                let modelName2 = arrModel[0];
                let fieldName = element.name;
                if (arrModel.length > 1) {
                    fieldName = arrModel[1];
                }
                // создаём прокси
                object[modelName2] = kolos.Utils.createProxy(object[modelName2]);
                let model = object[modelName2];
                kolos.Utils.listenValue(model, fieldName, (value) => {
                    if (element.type === "checkbox") {
                        $(element).prop("checked", kolos.Utils.isTrue(value));
                    } else if (element.type === "radio") {
                        if ($(element).val() == value) {
                            $(element).prop("checked", true);
                        } else {
                            $(element).prop("checked", false);
                        }
                    } else {
                        $(element).val(value);
                    }
                });
                $(element).on("input",function(ev){
                    if (element.type === "checkbox") {
                        model[fieldName] = $(element).prop("checked");
                    } else if (element.type === "radio") {
                        model[fieldName] = $(element).val();
                    } else {
                        model[fieldName] = $(element).val();
                    }
                });
            } else if (element.tagName == 'FORM') {
                let arrModel = modelName.split('.');
                let modelName2 = arrModel[0];
                // создаём прокси
                object[modelName2] = kolos.Utils.createProxy(object[modelName2]);
                let model = object[modelName2];
                // инициализируем форму, и прикрепляем к ней модель
                let form = new kolos.Form(element);
                form.attach(model);
                return form;
            } else {
                let arrModel = modelName.split('.');
                if (arrModel.length < 2) {
                    return;
                }
                let modelName2 = arrModel[0];
                let fieldName = arrModel[1];
                object[modelName2] = kolos.Utils.createProxy(object[modelName2]);
                let model = object[modelName2];
                kolos.Utils.listenValue(model, fieldName, (value) => {
                    $(element).html(value);
                });
            }
        }
    },


};