kolos.Form = function (tagId) {
    var Self = this;
    this.tagId = tagId;

    this._elements = {};
    this._onSubmit = undefined;

    this.onSubmit = function (callback) {
        this._onSubmit = callback;
    }

    this.onsubmit = function (callback) {
        this._onSubmit = callback;
    }

    this._init = function() {
        $(Self.tagId).submit(() => {
            if (Self._onSubmit !== undefined) {
                Self._onSubmit(Self);
            }
            return false;
        });

        //запоминаем все элементы
        $(Self.tagId).find('input, textarea, select').each(function() {
            //для radio отдельная логика
            if ($(this).prop("type") === 'radio') {
                //радио чекеды складываем в массив по значениям
                if (Self._elements[this.name] === undefined) {
                    Self._elements[this.name] = {
                        isArray: true,
                        items: []
                    };
                }
                Self._elements[this.name].items.push(this);
            } else {
                //остальные складываем как обычно
                Self._elements[this.name] = this;
            }
        });
    };

    this._init();


    this.getData = function() {
        var data = {};
        for (var name in this._elements) {
            //достаём все значения по имени
            data[name] = this.get(name);
        }
        return data;
    };

    this.getAll = function () {
        return this.getData();
    }

    this.get = function (name) {
        if (this._elements[name] !== undefined) {
            var element = this._elements[name];
            //если это массив radio
            if (element['isArray'] !== undefined) {
                for (var i in element.items) {
                    var radio = element.items[i];
                    //возвращаем значение выбранного элемента
                    if ($(radio).prop("checked")) {
                        return $(radio).val()
                    }
                }
            } else {
                //иначе это обычный элемент
                //если это чекбокс
                if ($(element).prop("type") === 'checkbox') {
                    if ($(element).prop("checked")) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return $(element).val();
                }
            }
        }
        return undefined;
    };

    // this.getElement = function (name) {
    //     if (this._elements[name] !== undefined) {
    //         return this._elements[name];
    //     }
    //     return undefined;
    // };

    this.setData = function (data) {
        for (let key in data) {
            this.set(key, data[key]);
        }
    }

    this.set = function (name, value) {
        if (this._elements[name] !== undefined) {
            var element = this._elements[name];
            //если это массив radio
            if (element['isArray'] !== undefined) {
                //иначе это массив radio
                for (var i in element.items) {
                    var radio = element.items[i];
                    //выставляем чек для заданного значения
                    if ($(radio).val() == value) { //тут обязательно неявное сравнение, чтобы типы преобразовывались автоматом
                        $(radio).prop('checked', true);
                    }

                }
            } else {
                if ($(element).prop("type") === 'checkbox') {
                    $(element).prop("checked", kolos.Utils.isTrue(value));
                } else {
                    $(element).val(value);
                }
            }
        }
    };

    this.attach = function (model) {
        model = kolos.Utils.createProxy(model);

        let elements = [];
        for (let name in this._elements) {
            let element = this._elements[name];
            if (element.isArray !== undefined) { // это radio
                for (let i in element.items) {
                    elements.push({
                        name: name,
                        element: element.items[i]
                    });
                }
            } else {
                elements.push({
                    name: name,
                    element: element
                });
            }
        }

        for (let i in elements) {
            let fieldName = elements[i].name;
            let element = elements[i].element;

            if (model[fieldName] != undefined) {

                // следим за изменением значения в поле ввода
                $(element).on("input",function(ev){
                    model[fieldName] = Self.get(fieldName);
                });

                // следим за изменением значения в поле модели
                kolos.Utils.listenValue(model, fieldName, (value) => {
                    Self.set(fieldName, value);
                });
            }
        }

        return model;
    }

    this.setStateSuccess = function (name) {
        this.setState(name, 'success');
    }

    this.setStateError = function (name) {
        this.setState(name, 'error');
    }

    this.setStateInfo = function (name) {
        this.setState(name, 'info');
    }

    this.setStateWarning = function (name) {
        this.setState(name, 'warning');
    }

    this.setState = function (name, state) {
        this.clearState(name);
        let el = this._elements[name];
        $(el).addClass(state);
    }

    this.clearState = function (name) {
        let el = this._elements[name];
        $(el).removeClass('success');
        $(el).removeClass('info');
        $(el).removeClass('warning');
        $(el).removeClass('error');
    }

    this.clearStateAll = function () {
        for (let name in this._elements) {
            this.clearState(name);
        }
    }

    this.toJson = function() {
        return JSON.stringify(this.getAll());
    }

};


// static method
/**
 * @param tagId
 * @return {kolos.Form}
 */
kolos.Form.init = function (tagId) {
    return new kolos.Form(tagId);
}
