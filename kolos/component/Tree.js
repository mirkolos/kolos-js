kolos.component.Tree = function() {
    // base properties -->>
    let Self = this;
    /** @type {kolos.ComponentContext} */
    this.context = {};
    this.element = {
        content: {},
    };
    this.component = {
        /** @type {kolos.component.Hint} */
        hint: {},
    }
    this.param = {
        fieldId: 'id',
        fieldParentId: 'parentId',
        fieldCaption: 'name',
        open: true,
    };
    this.attr = {};
    //--


    /**  @type {[kolos.component.Tree.Node]} */
    this.items = {};
    /**  @type {[kolos.component.Tree.Node]} */
    this.nodes = [];
    /** @type {kolos.component.Tree.Node} */
    this.selected = undefined;
    this.actions = [];

    this.__onSelectCallback = undefined;

    this.add = function (rowData) {
        let id = rowData[this.param.fieldId];
        // создаём узел
        let node = new kolos.component.Tree.Node(this);
        node.isOpen = true;
        node.data = rowData;
        this.items[id] = node;
        //--
        /** @type {kolos.component.Tree.Node} */
        let parentNode = this.items[node.getParentId()];
        if (parentNode !== undefined) {
            parentNode.child.push(node);
        } else {
            this.nodes.push(node);
        }
        //--
        node.buildElement();
        if (parentNode !== undefined) {
            parentNode.element.child.append(node.element.node);
            parentNode.open();
        } else {
            $(this.element.content).append(node.element.node);
        }
        this.update();
    }

    this.update = function () {
        for (let i in this.items) {
            let node = this.items[i];
            node.updateState();
        }
    }

    this.setData = function (data) {
        let isOpen = kolos.Utils.parseBoolean(Self.param.open);

        // создаём элементы
        for (let i in data) {
            let rowData = data[i];
            let id = rowData[this.param.fieldId];
            // создаём узел
            let node = new kolos.component.Tree.Node(this);
            node.isOpen = isOpen;
            node.data = rowData;
            this.items[id] = node;
        }

        // строим дерево
        this.nodes = [];
        for (let key in this.items) {
            /** @type {kolos.component.Tree.Node} */
            let node = this.items[key];
            /** @type {kolos.component.Tree.Node} */
            let parent = this.items[node.getParentId()];
            if (parent !== undefined) {
                parent.child.push(node);
            } else {
                this.nodes.push(node);
            }
        }

        $(this.element.content).html(this.__renderTree());
    }

    this.__renderTree = function () {
        let jqElement = $('<div></div>');

        // строим dom элементы
        for (let key in this.items) {
            /** @type {kolos.component.Tree.Node} */
            let node = this.items[key];
            let nodeElement = node.buildElement();
            jqElement.append(nodeElement);
        }

        // формируем дерево
        for (let key in this.items) {
            /** @type {kolos.component.Tree.Node} */
            let node = this.items[key];

            for (let i in node.child) {
                let childNode = node.child[i];
                $(node.element.child).append(childNode.element.node);
            }
        }

        return jqElement.children();

    }

    /**
     * @param callback (node) => { ... }
     */
    this.onSelect = function (callback) {
        Self.__onSelectCallback = callback;
    }

    this.__onSelect = function (node) {
        if (Self.selected !== undefined) {
            Self.selected.unselect();
        }
        this.selected = node;
        node.select();
        if (Self.__onSelectCallback !== undefined) {
            Self.__onSelectCallback(node);
        }
    }

    this.select = function (id) {
        let node = Self.getNode(id);
        if (node === undefined) {
            return;
        }
        this.__onSelect(node);
    }

    this.openAll = function () {
        for (let i in this.items) {
            this.items[i].open();
        }
    }

    this.closeAll = function () {
        for (let i in this.items) {
            this.items[i].close();
        }
    }

    /**
     *
     * @param caption
     * @param {function} callback в параметры передаётся узел
     */
    this.addAction = function (caption, callback) {
        this.actions.push({
            'caption': caption,
            'callback': callback
        });
    }

    /** @returns {kolos.component.Tree.Node} */
    this.getNode = function (id) {
        return this.items[id];
    }

    this.delete = function (id) {
        let node = this.getNode(id);
        if (node === undefined) {
            return [];
        }
        return node.delete();
    }

    this.getTreeNodeIds = function(id) {
        let nodes = this.getTreeNodes(id);
        let result = [];
        for (let i  in nodes) {
            let node = nodes[i];
            result.push(node.getId());
        }
        return result;
    }

    /**
     * @returns {[kolos.component.Tree.Node]}
     */
    this.getTreeNodes = function (id) {
        let list = [];
        // добавляем текущий узел
        list.push(this.getNode(id));
        list = this.getChildNodes(id, list);
        return list;
    }

    /**
     * @returns {[kolos.component.Tree.Node]}
     */
    this.getChildNodes = function (id, list) {
        if (list === undefined) {
            list = [];
        }
        let node = this.getNode(id);
        for (let i in node.child) {
            let childNode = node.child[i];
            list.push(childNode);
            list = this.getChildNodes(childNode.getId(), list);
        }
        return list;
    }

    this.onReady = function() {
        //
    }

    this.onDestroy = function() {
        //
    }

    // this.template = function() {
    //     return `
    //         <div>
    //             <div component="kolos.component.Hint"></div>
    //             <div id="link">
    //                 <a href="javascript: kolos.component.Tree.exampleMethod();">Test exampleMethod</a>
    //             </div>
    //             <div>[[content]]</div>
    //         </div>
    //     `;
    // }
};

kolos.component.Tree.Node = function (parentCom) {
    let Self = this;
    /** @type {kolos.component.Tree} */
    this.parentCom = parentCom;
    // dom элементы
    this.element = {
        node: undefined,
        caption: undefined,
        arrow: undefined,
        icon: undefined,
        text: undefined,
        child: undefined,
        actions: undefined
    };
    /** @type [{kolos.component.Tree.Node}] */
    this.child = [];
    this.data = {};
    this.isOpen = true;

    this.getId = function () {
        return this.data[parentCom.param.fieldId];
    }

    this.getParentId = function () {
        return this.data[parentCom.param.fieldParentId];
    }

    this.getCaption = function () {
        return this.data[parentCom.param.fieldCaption];
    }

    this.open = function () {
        if (!Self.isOpen) {
            Self.isOpen = true;
            $(Self.element.arrow).html('expand_more');
            $(Self.element.child).slideDown(150);
        }
    }

    this.close = function () {
        if (Self.isOpen) {
            Self.isOpen = false;
            $(Self.element.child).slideUp(150, () => {
                $(Self.element.arrow).html('chevron_right');
            });
        }
    }

    this.select = function () {
        $(Self.element.caption).addClass('sel');
    }

    this.unselect = function () {
        $(Self.element.caption).removeClass('sel');
    }

    /**
     * @returns {*[]} return list deleted ids
     */
    this.delete = function ()  {

        let deleted = [];
        deleted.push(this.getId());

        // 1) удаляем дочерние узлы
        let childNodes = [].concat(this.child);
        for (let i in childNodes) {
            let node = childNodes[i];
            let ids = node.delete();
            deleted = deleted.concat(ids);
        }

        // 2) удаляем из родительского компонента
        let parentNode = this.parentCom.getNode(this.getParentId());
        if (parentNode !== undefined) {
            for (let i = parentNode.child.length - 1; i >= 0;  i--) {
                let node = parentNode.child[i];
                if (node !== undefined && node.getId() == this.getId()) {
                    parentNode.child.splice(i, 1);
                    break;
                }
            }
        }

        // 3) удаляем из данных
        delete this.parentCom.items[this.getId()];

        // 4) удаляем структуры в компоненте (если он корневой)
        for (let i = this.parentCom.nodes.length - 1; i >=0; i--) {
            let node = this.parentCom.nodes[i];
            if  (node !== undefined && node.getId() == this.getId()) {
                this.parentCom.nodes.slice(i, 1);
                break;
            }
        }

        // 5) удаляем DOM элемент
        $(this.element.node).remove();

        // 6) проверяем, если объект в выделен, то очищаем его
        if (this.parentCom.selected !== undefined && this.parentCom.selected.getId() === this.getId()) {
            this.parentCom.selected = undefined;
        }

        return deleted;
    }

    this.onclick = function () {
        // вызываем обработчик клика дерева
        if (Self.parentCom.__onSelect !== undefined) {
            Self.parentCom.__onSelect(Self);
        }

        // открытие
        if (Self.child.length > 0) {
            if (!Self.isOpen) {
                Self.open();
            }
        }
    }

    this.onclickarrow = function () {
        // открытие
        if (Self.child.length > 0) {
            if (Self.isOpen) {
                Self.close();
            } else {
                Self.open();
            }
        }
    }

    this.buildElement = function () {
        let jqElement = $(Self.__templateNode(Self));
        Self.element.node = jqElement[0];
        Self.element.caption = $(jqElement).find('.tr-caption')[0];
        Self.element.text = $(jqElement).find('.tr-text')[0];
        Self.element.child = $(jqElement).find('.tr-child')[0];
        Self.element.arrow = $(jqElement).find('.tr-arrow')[0];
        Self.element.icon = $(jqElement).find('.tr-icon')[0];
        Self.element.actions = $(jqElement).find('.tr-actions')[0];

        Self.element.caption.onclick = function (e) {
            Self.onclick(e);
            e.stopPropagation();
        }

        Self.element.arrow.onclick = function (e) {
            Self.onclickarrow(e);
            e.stopPropagation();
        }

        Self.element.actions.onclick = function (e) {
            e.stopPropagation();
        }
        Self.element.actions.onmousedown = function (e) {
            e.stopPropagation();
        }

        for (let i in Self.parentCom.actions) {
            let action = Self.parentCom.actions[i];
            let actionElement = $('<a href="javascript: void(0);">' + action['caption'] + '</a>')[0];
            actionElement.onclick = function (e) {
                action.callback(Self);
            }
            $(Self.element.actions).prepend(actionElement);
        }

        Self.updateState();

        return Self.element.node;
    }

    this.updateState = function () {

        $(Self.element.text).html(Self.getCaption());

        if (Self.child.length === 0) {
            $(Self.element.icon).show();
            $(Self.element.arrow).hide();
        } else {
            $(Self.element.icon).hide();
            $(Self.element.arrow).show();
        }

        if (Self.isOpen) {
            $(Self.element.arrow).html('expand_more');
        } else {
            $(Self.element.arrow).html('chevron_right');
        }
    }

    /**
     * @param {kolos.component.Tree.Node} node
     * @return {string}
     * @private
     */
    this.__templateNode = function (node) {
        let cls = '';

        if (!Self.isOpen) {
            cls += ' hide \n';
        }

        return `<div class="tr-node">
            <div class="tr-caption" onmousedown="event.stopPropagation();">
                <a class="icon-m tr-arrow" href="javascript: void(0);" 
                    onmousedown="event.stopPropagation();"
                    style=" margin-top: -3px; display: inline-block;">expand_more</a>
                <!--<a class="icon-m" href="javascript: void(0);">check_box_outline_blank</a>-->
                <i class="icon-m tr-icon">feed</i><span class="tr-text"> ` + node.getCaption() + `</span>
                <span class="tr-actions"></span>
            </div>
            <div class="tr-child ` + cls + `">
            </div>
        </div>`;
    }

};
