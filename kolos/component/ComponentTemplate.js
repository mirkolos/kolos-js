kolos.component.ComponentTemplate = function() {
    // base properties -->>
    let Self = this;
    /** @type {kolos.ComponentContext} */
    this.context = {};
    this.element = {
        link: {},
    };
    this.component = {
        /** @type {kolos.component.Hint} */
        hint: {},
    }
    this.param = {};
    this.attr = {};
    //--


    // пользовательский метод
    this.exampleMethod = function() {
        alert('123');
    }

    this.onPage = function () {
        // когда компонент используется как страница
        // вызывается при переходе на страницу
    }

    this.onInit = function () {
        // вызывается перед выводом шаблона
    }

    this.onReady = function() {
        // вызывается когда компонент полностью готов, готовы вложенные компоненты, и отрисовался шаблон
    }

    this.onDestroy = function() {
        // при уничтожении компонента
    }

    // Шаблон. Если не создавать данный метод, то будет загружаться файл ComponentTemplate.html в качестве шаблона
    this.template = function() {
        return `
            <div>
                <div component="kolos.component.Hint"></div>
                <div id="link">
                    <a href="javascript: kolos.component.ComponentTemplate.exampleMethod();">Test exampleMethod</a>
                </div>
                <div>[[content]]</div>
            </div>
        `;
    }
}

