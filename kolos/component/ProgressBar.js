kolos.component.ProgressBar = function () {
    let Self = this;
    /** @type {kolos.ComponentContext} */
    this.context = {};
    this.element = {
        caption: {}
    };
    this.attr = {
        type: undefined,
    }
    this.param = {
        type: undefined,
    }

    this._progress = 0;
    this._percent = 0;
    this._min = 0;
    this._max = 100;
    this._type = 'info';

    this.setProgress = function (progress, caption) {
        this._progress = progress;
        let percent = Math.round(this._progress * 100 / (this._max - this._min));
        if (percent < 0) {
            percent = 0;
        }
        if (percent > 100) {
            percent = 100;
        }
        // не даём выставлять 100%, если не достигли максимума
        if (percent === 100 && progress < this._max) {
            percent = 99;
        }
        this._percent = percent;
        //--
        $(this.element.caption).width(this._percent + '%');
        if (caption !== undefined) {
            this.setCaption(caption);
        } else {
            this.setCaption(this._percent + '%');
        }
    }

    this.setMax = function (max) {
        this._max = max;
    }

    this.setMin = function (min) {
        this._min = min;
    }

    this.setCaption = function (caption) {
        $(this.element.caption).html(caption);
    }

    this.setType = function (type) {
        $(this.context.element).removeClass(this._type);
        $(this.context.element).addClass(type);
        this._type = type;
    }

    this.onReady = function () {
        let type = '';
        if (!kolos.Utils.isEmpty(this.attr.type)) {
            type = this.attr.type;
        }
        if (!kolos.Utils.isEmpty(this.param.type)) {
            type = this.param.type;
        }
        if (!kolos.Utils.isEmpty(type)) {
            this.setType(type);
        }
        this.setProgress(0);
    }

    this.template = function () {
        return '<div class="progress-bar"><div id="caption" class="progress"></div></div>';
    }
}